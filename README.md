Ateliers Python au BIB
======================

Ce dépôt contient les projets, ressources et documents réalisés dans le cadre des ateliers "Python" au BIB. Ces atelies d'initiation à la programmation, présentent divers sujets liés à la programmation en utilisant Python comme support : les concepts étudiés ne sont pas forcèment spécifiques au langage Python.

## Documents (supports des ateliers)

Ces documents sont rédigés avec `org-mode`, et exportés (par `LaTeX`) en plusieurs formats pour pouvoir être lus sur plusieurs médias différents.

**NOTE:** N'hésitez pas à modifier ces documents et à proposer des améliorations, elle seront acceptées avec **grand plaisir**. Pour ce faire, vous pouvez cloner le dépôt et soumettre une demande de fusion, ou tout simplement m'envoyer une version modifiée par mail. Les modifications doivent idéalement être réalisées sur le fichier `.org`, mais si vous ne sentez pas à l'aise avec ce format, vous pouvez envoyer une version modifiée du document ODT.

### Ateliers d'initiation à la programmation

 - #1 : **Qu'est-ce qu'un langage de programmation ?** Un atelier sur les grammaires formelles, pour mieux comprendre la notion de "langage de programmation" ([PDF](./documents/atelier-0001.pdf), [HTML](./documents/atelier-0001.html), [ODT](./documents/atelier-0001.odt), [TXT](./documents/atelier-0001.txt), [org](./documents/atelier-0001.org))
  - #2 : **Qu'est-ce qu'un ordinateur ?** Un atelier sur les machines de Turing, pour mieux comprendre la notion "d'ordinateur" et de "programme" ([PDF](./documents/atelier-0002.pdf), [HTML](./documents/atelier-0002.html), [ODT](./documents/atelier-0002.odt), [TXT](./documents/atelier-0002.txt), [org](./documents/atelier-0002.org))
  - #3 : **Comment représenter des données structurées ?** Une introduction aux types de données de Python, pour pouvoir représenter des données scalaires et non-scalaires ([PDF](./documents/atelier-0003.pdf), [HTML](./documents/atelier-0003.html), [ODT](./documents/atelier-0003.odt), [TXT](./documents/atelier-0003.txt), [org](./documents/atelier-0003.org))
  - #4 : **Comment représenter des données hiérarchiques ?** Une introduction aux arbres et aux graphes en Python, pour pouvoir représenter des données hiérarchiques (**pas encore terminé**, [org](./documents/atelier-0004.org))

### Ateliers annexes

  - **Comment ne pas faire du Python ?** Un atelier amusant sur le lambda-calcul et la fonction factorielle, ou l'on écrit un algorithme en s'interdisant quasiment tout les fonctionnalités du langage Python ([PDF](./documents/anti-programmation.pdf), [HTML](./documents/anti-programmation.html), [org](./documents/anti-programmation.org))
  - **Une étude algorithmique de la fonction factorielle.** Un atelier partiellement basé sur l'atelier précédent, ou l'on étudie la fonction factorielle de manière plus large, en observant le comportement de plusieurs écritures différentes, ainsi que l'écriture en lambda-calcul décrite dans l'atelier précédent, puis en écrivant notre propre interpréteur de lambda-calcul sous la forme d'une machine de Krivine, qui nous sert à écrire notre fonction sous la forme de combinateurs SKI, afin de pouvoir l'écrire à l'aide du pseudo-combinateur Iota, et de définir un codage de Gödel basé sur ce pseudo-combinateur puis d'écrire notre fonction dans ce codage. Ensuite, nous implémentons une machine de Turing permettant de calculer la factorielle, et nous utilisons ces deux implémentations pour étudier la complexité algorithmique de cette fonction. **Cet atelier n'est pas terminé, et je cherche de l'aide pour l'achever.** ([PDF](./documents/etude-fonction-factorielle.pdf), [HTML](./documents/etude-fonction-factorielle.html), [org](./documents/etude-fonction-factorielle.org))
